# ================================================
# Skeleton codes for HW5
# Read the skeleton codes carefully and put all your
# codes into function "reconstruct_from_binary_patterns"
# ================================================
import sys
import cv2
import numpy as np
from math import log, ceil, floor
import matplotlib.pyplot as plt
import pickle


points_3d_color = []
def help_message():
    # Note: it is assumed that "binary_codes_ids_codebook.pckl", "stereo_calibration.pckl",
    # and images folder are in the same root folder as your "generate_data.py" source file.
    # Same folder structure will be used when we test your program

    print("Usage: [Output_Directory]")
    print("[Output_Directory]")
    print("Where to put your output.xyz")
    print("Example usages:")
    print(sys.argv[0] + " ./")

def reconstruct_from_binary_patterns():
    scale_factor = 1.0
    ref_white = cv2.resize(cv2.imread("images/pattern000.jpg", cv2.IMREAD_GRAYSCALE) / 255.0, (0,0), fx=scale_factor,fy=scale_factor)
    ref_black = cv2.resize(cv2.imread("images/pattern001.jpg", cv2.IMREAD_GRAYSCALE) / 255.0, (0,0), fx=scale_factor,fy=scale_factor)
    ref_avg   = (ref_white + ref_black) / 2.0
    ref_on    = ref_avg + 0.05 # a threshold for ON pixels
    ref_off   = ref_avg - 0.05 # add a small buffer region

    h,w = ref_white.shape

    # mask of pixels where there is projection
    proj_mask = (ref_white > (ref_black + 0.05))

    scan_bits = np.zeros((h,w), dtype=np.uint16)

    im_color = cv2.resize(cv2.imread("images/pattern001.jpg", cv2.IMREAD_COLOR) , (0,0),fx=scale_factor, fy=scale_factor)
    # analyze the binary patterns from the camera
    for i in range(0,15):
        # read the file
        patt_gray = cv2.resize(cv2.imread("images/pattern%03d.jpg"%(i+2), cv2.IMREAD_GRAYSCALE)/255.0, (0,0), fx=scale_factor,fy=scale_factor)

        # mask where the pixels are ON
        on_mask = (patt_gray > ref_on) & proj_mask

        # this code corresponds with the binary pattern code
        bit_code = np.uint16(1 << i)
        for x in range(w):
            for y in range(h):
                if on_mask[y][x] == True:
                    scan_bits[y][x] += bit_code

        # TODO: populate scan_bits by putting the bit_code according to on_mask

    # the codebook translates from <binary code> to (x,y) in projector screen space
    with open("binary_codes_ids_codebook.pckl","r") as f:
        binary_codes_ids_codebook = pickle.load(f)

    #print binary_codes_ids_codebook
    camera_points = []
    projector_points = []
    color_points = []
    data = np.zeros((h, w, 3), dtype=float)
    for x in range(w):
        for y in range(h):
            if not proj_mask[y,x]:
                continue # no projection here
            if scan_bits[y,x] not in binary_codes_ids_codebook:
                continue # bad binary code
            p_x, p_y = binary_codes_ids_codebook[scan_bits[y,x]]
            if p_x >= 1279 or p_y >= 799: # filter
                continue
            data[y, x] = [0, p_y*255/799.0, p_x*255/1279.0]
            camera_points.append([[y/2.0, x/2.0]])
            projector_points.append([[p_y, p_x]])
            color_points.append(im_color[y, x])

    cv2.imwrite('correspondence.jpg', data)

    # load the prepared stereo calibration between projector and camera
    with open("stereo_calibration.pckl","r") as f:
        d = pickle.load(f)
        camera_K    = d['camera_K']
        camera_d    = d['camera_d']
        projector_K = d['projector_K']
        projector_d = d['projector_d']
        projector_R = d['projector_R']
        projector_t = d['projector_t']

    ud_cp = cv2.undistortPoints(np.array(camera_points, dtype=np.float32), camera_K, camera_d)
    ud_pp = cv2.undistortPoints(np.array(projector_points, dtype=np.float32), projector_K, projector_d)

    pr1 = np.eye(3, 4).astype(np.float32)
    pr2 = np.concatenate((projector_R,  projector_t), axis=1)

    TP = cv2.triangulatePoints(pr1[:3], pr2, ud_cp, ud_pp)
    output = cv2.convertPointsFromHomogeneous(TP.T)
 
    points_3d = np.array(output)
    len_points_3d = points_3d.shape[0]
    updated_points_3d = []
    for i in range(len(points_3d)):
        if points_3d[i][0][2] >= 200 and points_3d[i][0][2] <= 1400:
            updated_points_3d.append(points_3d[i])
            points_3d_color.append(color_points[i])
    updated_points_3d = np.array(updated_points_3d)
    return updated_points_3d
	
def write_3d_points(points_3d):
    
# ===== DO NOT CHANGE THIS FUNCTION =====

    output_name = sys.argv[1] + "output.xyz"
    with open(output_name,"w") as f:
        for p in points_3d:
            f.write("%d %d %d\n"%(p[0,0],p[0,1],p[0,2]))

    output_name = sys.argv[1]+"output_color.xyz"
    len_points_3d = points_3d.shape[0]
    with open(output_name, 'w') as f:
        for i in range(len_points_3d):
            f.write('%d %d %d %d %d %d\n'%(points_3d[i,0,2], points_3d[i,0,1], points_3d[i,0,0], points_3d_color[i][2], points_3d_color[i][1], points_3d_color[i][0]))
    return points_3d#, camera_points, projector_points
    
if __name__ == '__main__':

	# ===== DO NOT CHANGE THIS FUNCTION =====
	
	# validate the input arguments
    if (len(sys.argv) != 2):
        help_message()
        sys.exit()

    points_3d = reconstruct_from_binary_patterns()
    write_3d_points(points_3d)
	
